def squareroot(x):
    x0 = [x]
    #100 iterations to be sure
    for i in range(0,100):
        x0.append((x0[i]+x/x0[i])/2)
    return float(x0[99])
